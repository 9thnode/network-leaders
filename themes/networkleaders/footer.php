<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_sidebar( 'footerfull' ); ?>

<div class="wrapper" style="padding: 0 0" id="wrapper-footer">

			<div class="col-md-12" style="background-color: #919090;">

				<footer class="site-footer" id="colophon">

					<div class="site-info text-center pt-2 pb-2">

						<span class="align-middle"><a href="https://networkleader.com" title="Network Leader Diagnostic">Network Leader Diagnostic</a> &copy; <?php echo date("Y"); ?> - All Rights Reserved | Website development by: <a href="https://designbreakstudios.com" title="Design Break Studios" target="_blank">Design Break Studios</a> | Powered by: <a href="http://9thnode.com" title="9th Node Networks" target="_blank">9th Node Networks</a></span>

					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>
