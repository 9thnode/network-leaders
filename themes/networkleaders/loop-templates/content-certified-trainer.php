<?php
/**
* Partial template for content in certified-trainer.php *
* @package understrap */
$container = get_theme_mod( 'understrap_container_type' );
?>
<article <?php post_class(); ?> id="post-
    <?php the_ID(); ?>">

    <div class="entry-content">

        <div class="container">
            <div class="row justify-content-center pb-5">
                <?php the_content(); ?>
            </div>
        </div>

        <!--- Data Driven Grid --->
        <div class="<?php echo esc_attr( $container ); ?>">
            <div class="row pt-5 mb-5">

                <?php if( have_rows( 'trainer_3rd_section') ): while( have_rows( 'trainer_3rd_section') ): the_row();
                // vars
                    $title=get_sub_field( 'trainer_3rd_title');
                    $icon=get_sub_field( 'trainer_3rd_icon');
                    $content=get_sub_field( 'trainer_3rd_content_right');
                ?>
                <div class="col-md-5 bottom-border">
                    <h2 class="p-2 text-center data-title"><?php echo $title ?></h2>
                    <center><img src="<?php echo $icon ?>" alt="<?php echo $title ?>" />
                    </center>
                </div>
                <div class="col-md-7 pl-5-md mobile-bullets">
                    <span style="font-size: 20px; line-height: 2em;"><?php echo $content ?></span>
                </div>
                <?php endwhile; ?>

                <?php endif; ?>

            </div>
        </div>

        <!--- Network Patterns 4 box grid --->
        <div class="<?php echo esc_attr( $container ); ?>">
            <div class="row justify-content-center pt-5 ">
                <div class="col-10 text-center">
                    <?php the_field( 'trainer_3_box_header_text'); ?>
                </div>
                <div class="row justify-content-center mt-5 mb-5">
                    <?php if( have_rows( 'organization_improve_icons') ): while( have_rows( 'organization_improve_icons') ): the_row();
                    // vars
                        $title=get_sub_field( 'trainer_3_box_title');
                        $icon=get_sub_field( 'trainer_3_box_icon');
                        $subtitle=get_sub_field( 'trainer_3_box_subtitle');
                    ?>
                    <div class="col-12 col-md-4 pl-5 pr-5">
                        <h3 class="text-center" style="font-weight: normal"><?php echo $title ?></h3>
                        <center><img src="<?php echo $icon ?>" alt="<?php echo $title ?>" class="mb-4" />
                        </center>
                        <h5 class="text-center" style="font-weight: normal"><?php echo $subtitle ?></h5>
                    </div>

                    <?php endwhile; ?>

                    <?php endif; ?>
                </div>
            </div>
        </div>

        <!--- Pricing CTA --->

        <?php if( have_rows( 'pricing_cta') ): while( have_rows( 'pricing_cta') ): the_row(); // vars
            $title=get_sub_field( 'pricing_cta_title');
            $btnlabel=get_sub_field( 'pricing_cta_button_label');
            $link=get_sub_field( 'pricing_cta_button_link');
            $color=get_sub_field( 'pricing_cta_background_color');
        ?>

        <div class="row mt-5 mb-5" style="background-color: <?php echo $color ?>;">
            <div class="col-12 col-md-8 pl-md-5 ml-md-5 text-white cta-title">
                <?php echo $title ?>
            </div>
            <div class="col-12 col-md-3 mb-3 mb-md-0 d-flex align-items-center">
                <a href="<?php echo $link ?>" class="btn btn-secondary btn-lg cta-btn col-sm-12" style="color: <?php echo $color ?>" title="<?php echo $btnlabel ?>">
                    <?php echo $btnlabel ?>
                </a>
            </div>
        </div>

        <?php endwhile; ?>

        <?php endif; ?>

        <!--- Video Section --->

    		<?php if( have_rows('trainer_video') ):

    			while( have_rows('trainer_video') ): the_row();

    							// vars
    							$background = get_sub_field('trainer_vid_background');
    							$title = get_sub_field('trainer_vid_title');
    							$btn_text = get_sub_field('trainer_vid_btn_text');
    							$btn_url = get_sub_field('trainer_vid_btn_url');

    			?>

    			<div class="row mt-5 mb-5 pt-5 pb-5 trainer-video" style="background-image: url('<?php echo $background ?>');">

    					<div class="container">
    						<div class="row justify-content-end">
    							<div class="col-md-6 mt-5 mb-5 pt-5 pb-5">
    								<h2 class="pt-5 pb-5 text-white"><?php echo $title ?></em></h2>
    								<a href="<?php echo $btn_url ?>" class="btn-primary btn-lg text-white mt-3 mb-5 p-3 wplightbox"><?php echo $btn_text ?></a>
    							</div>
    						</div>
    					</div>
    					<?php endwhile; ?>

    				<?php endif; ?>

    			</div>

    </div>
    <!-- .entry-content -->

</article>
<!-- #post-## -->
