<?php
/**
* Partial template for content in certified-trainer-backend.php *
* @package understrap */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<article <?php post_class(); ?> id="post-
    <?php the_ID(); ?>">

    <div class="entry-content">

        <div class="<?php echo esc_attr( $container ); ?>">
            <div class="row justify-content-center pb-5">
                <?php the_content(); ?>
            </div>
        </div>

        <div class="<?php echo esc_attr( $container ); ?> intro  mt-5 mb-5">
            <h2 class="text-center">Getting Started Series</h2>
            <p>Review the videos below, which introduce the initial content on Leader Network Diagnostic to users. These videos will help you frame your debrief and training sessions. Some of these videos are also used in your certified training slide shows. We suggest you take the diagnostic yourself before you begin learning how to facilitate the program. You can jump over to the page your participants will use to complete the diagnostic by clicking here. </p>
        </div>

            <div id="startedCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner row w-100 mx-auto" role="listbox">
                    <div class="carousel-item col-md-4 active">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Networks-101.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/Networks-101-Thumbnail.jpg" alt="Networks-101 Video">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Networks 101</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 1 of 5 - (6:50)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Step-01-Who-is-in-your-Network.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/Step-01-Thumbnail.jpg" alt="Step 1">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Who Is In Your Network</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 2 of 5 - (3:57)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Step-02-How-Open-or-Closed.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/Step-02-Thumbnail.jpg" alt="Step 2">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>How Open or Closed</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 3 of 5 - (6:48)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Step-03-Diversity-and-Depth.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/Step-03-Thumbnail.jpg" alt="Step 3">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Diversity and Depth</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 4 of 5 - (8:59)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Step-04-Simple-Network-Changes.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/Step-04-Thumbnail.jpg" alt="Step 4">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Simple Network Changes</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 5 of 5 - (4:27)
                        </div>
                      </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#startedCarousel" role="button" data-slide="prev">
                    <i class="fa fa-chevron-left fa-lg text-muted"></i>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next text-faded" href="#startedCarousel" role="button" data-slide="next">
                    <i class="fa fa-chevron-right fa-lg text-muted"></i>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        <div class="<?php echo esc_attr( $container ); ?> intro  mt-5 mb-5">
            <h2 class="text-center">Facilitation Guide Series</h2>
            <p>The videos below walk you through the best practices for delivering the Leader Network Diagnostic content to a group. Network Leader founder Phil Willburn offers specific tips and explanations to help you train leaders. </p>
        </div>

            <div id="facilitationCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner row w-100 mx-auto" role="listbox">
                    <div class="carousel-item col-md-4 active">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Facilitation-Guide-Video-1-The-Connect-Activity.mp4" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/Facilitation-guide-01.jpg" alt="Facilitation Guide 1">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>The Connect Activity</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 1 of 7 - (4:44)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Facilitation-Guide-Video-2-Agenda-Intro-to-Effective-Networks.mp4" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/Facilitation-guide-02.jpg" alt="Facilitation Guide 2">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Agenda/ Intro to Effective Networks</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 2 of 7 - (5:13)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Facilitation-Guide-Video-3-Diagnostic-Walkthrough.mp4" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/Facilitation-guide-03.jpg" alt="Facilitation Guide 3">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Diagnostic Walkthrough</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 3 of 7 - (3:46)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Facilitation-Guide-Video-4-Open-Networks.mp4" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/Facilitation-guide-04.jpg" alt="Facilitation Guide 4">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Open Networks</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 4 of 7 - (5:33)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Facilitation-Guide-Video-5-Diverse-and-Deep-Networks.mp4" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/Facilitation-guide-05.jpg" alt="Facilitation Guide 5">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Diverse and Deep Networks</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 5 of 7 - (7:24)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Facilitation-Guide-Video-6-Network-Traps.mp4" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/Facilitation-guide-06.jpg" alt="Facilitation Guide 6">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Network Traps</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 6 of 7 - (1:21)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Facilitation-Guide-Video-7-Supplemental-Material.mp4" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/Facilitation-guide-07.jpg" alt="Facilitation Guide 7">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Supplemental Material</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 7 of 7 - (2:49)
                        </div>
                      </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#facilitationCarousel" role="button" data-slide="prev">
                    <i class="fa fa-chevron-left fa-lg text-muted"></i>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next text-faded" href="#facilitationCarousel" role="button" data-slide="next">
                    <i class="fa fa-chevron-right fa-lg text-muted"></i>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        <div class="<?php echo esc_attr( $container ); ?> intro  mt-5 mb-5">
            <h2 class="text-center">Full Classroom Presentation</h2>
            <p>Get a sense of how the content and debrief come together in a classroom setting by watching the video series below, as Phil Willburn facilitates a session on the Leader Network Diagnostic. </p>
        </div>

            <div id="classroomCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner row w-100 mx-auto" role="listbox">
                    <div class="carousel-item col-md-4 active">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/LND-Walkthrough-01.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Walkthrough-Thumbs-01.jpg" alt="Walkthrough Video 1">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Walkthrough Video 1</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 1 of 10 - (2:35)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/LND-Walkthrough-02.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Walkthrough-Thumbs-02.jpg" alt="Walkthrough Video 2">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Walkthrough Video 2</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 2 of 10 - (10:42)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/LND-Walkthrough-03.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Walkthrough-Thumbs-03.jpg" alt="Walkthrough Video 3">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Walkthrough Video 3</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 3 of 10 - (8:52)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/LND-Walkthrough-04.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Walkthrough-Thumbs-04.jpg" alt="Walkthrough Video 4">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Walkthrough Video 4</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 4 of 10 - (4:15)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/LND-Walkthrough-05.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Walkthrough-Thumbs-05.jpg" alt="Walkthrough Video 5">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Walkthrough Video 5</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 5 of 10 - (11:04)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/LND-Walkthrough-06.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Walkthrough-Thumbs-06.jpg" alt="Walkthrough Video 6">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Walkthrough Video 6</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 6 of 10 - (4:26)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/LND-Walkthrough-07.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Walkthrough-Thumbs-07.jpg" alt="Walkthrough Video 7">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Walkthrough Video 7</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 7 of 10 - (12:23)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/LND-Walkthrough-08.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Walkthrough-Thumbs-08.jpg" alt="Walkthrough Video 8">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Walkthrough Video 8</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 8 of 10 - (4:36)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/LND-Walkthrough-09.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Walkthrough-Thumbs-09.jpg" alt="Walkthrough Video 9">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Walkthrough Video 9</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 9 of 10 - (3:25)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/LND-Walkthrough-10.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Walkthrough-Thumbs-10.jpg" alt="Walkthrough Video 10">
                      <a/>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Walkthrough Video 10</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 10 of 10 - (13:24)
                        </div>
                      </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#classroomCarousel" role="button" data-slide="prev">
                    <i class="fa fa-chevron-left fa-lg text-muted"></i>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next text-faded" href="#classroomCarousel" role="button" data-slide="next">
                    <i class="fa fa-chevron-right fa-lg text-muted"></i>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        <div class="<?php echo esc_attr( $container ); ?> intro  mt-5 mb-5">
            <h2 class="text-center">Network Traps</h2>
            <p>To help you better understand the science and deliver the content, we put together videos that provide further insight into different kinds of network traps users commonly fall into. </p>
        </div>

            <div id="networkTraps" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner row w-100 mx-auto" role="listbox">
                    <div class="carousel-item col-md-4 active">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Developmental-Traps.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Traps-01-Resource.jpg" alt="Networks-101 Video">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Part 1 - Developmental Traps</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 1 of 3 - (3:33)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Structural-Traps.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Traps-02-Structural.jpg" alt="Step 1">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Part 2 - Resource Traps</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 2 of 3 - (4:40)
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item col-md-4">
                      <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Resource-Traps.m4v" class="wplightbox">
                        <img class="img-fluid mx-auto d-block" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Traps-03-Developmental.jpg" alt="Step 2">
                      </a>
                      <div class="video-meta">
                        <div class="video-meta-title">
                          <strong>Part 3 - Structural Traps</strong>
                        </div>
                        <div class="video-meta-description">
                          Video 3 of 3 - (6:10)
                        </div>
                      </div>
                    </div>
                </div>
            </div>

        <!---Downloads-->
         <div class="<?php echo esc_attr( $container ); ?> intro p-5">
                <h2 class="text-center">Downloads</h2>
                <p>The documents below support the delivery of the Leader Network Diagnostic. Not only do you have access to the diagnostic, interpretation guide, and network traps guide, but as a trainer, you can also download PowerPoint presentations that we’ve designed to help you debrief this tool to a group. You have the option of using either the standard Diagnostic PowerPoint or the Accelerated PowerPoint. The Accelerated version is embedded with videos from the Network Leader team so that you can focus more on the facilitation and less on the network science. </p>
        </div>

<div class="container">
        <div class="row p-3">
            <div class="col-md-3 mb-3">
              <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/Leader-Network-Diagnostic.pdf" title="The Diagnostic (PDF)" target="_blank">
                <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/10/diagnostic.png"/>
              </a>
            </div>
            <div class="col-md-3 mb-3">
              <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/LeaderNetworkDiagnostic.pptx" title="Diagnostic PowerPoint (PPT)" target="_blank">
                <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/10/diagnostic-ppt.png"/>
              </a>
            </div>
            <div class="col-md-3 mb-3">
              <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Interpretation-Guide.pdf" title="Interpretation Guide (PDF)" target="_blank">
                <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/10/interpretation-guide.png"/>
              </a>
            </div>
            <div class="col-md-3 mb-3">
              <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/12/LND-Network-Traps.pdf" title="Network Traps (PDF)" target="_blank">
                <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/10/network-traps.png" />
              </a>
            </div>
            <div class="col-md-3 mb-3">
              <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/Accelerated_LND.pdf" title="Accellerated Diagnostic (PDF)" target="_blank">
                <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/10/accellerated-diagnostic.png"/>
              </a>
            </div>
            <div class="col-md-3 mb-3">
              <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/LeaderNetworkDiagnostic_Accelerated.pptx" title="Accellerated PowerPoint (PPT)" target="_blank">
                <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/10/accellerated-ppt.png"/>
              </a>
            </div>
            <div class="col-md-3 mb-3">
              <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2017/11/LND_FAQ.pdf" title="FAQ (PDF)" target="_blank">
                <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/10/faq.png"/>
              </a>
            </div>
        </div>
</div>
        <!---Downloads-->

    </div>
    <!-- .entry-content -->

</article>
<!-- #post-## -->

<script>
$('#facilitationCarousel').on('slide.bs.carousel', function (e) {

    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});

$('#classroomCarousel').on('slide.bs.carousel', function (e) {

    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});

$('#startedCarousel').on('slide.bs.carousel', function (e) {

    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});

$('#networkTraps').on('slide.bs.carousel', function (e) {

    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});
</script>
