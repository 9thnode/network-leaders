<?php
/**
 * Partial template for content in diagnostic.php
 *
 * @package understrap
 */
?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<div class="entry-content">

		<div class="container">
			<div class="row justify-content-center pb-5">
				<?php the_content(); ?>
			</div>
		</div>

		<!--- 4 box grid --->
		<div class="container">
			<div class="row mt-5 pt-5 mb-5 justify-content-center">

				<?php the_field('diag_title'); ?>

				<div class="row justify-content-center mt-5">
					<?php if( have_rows('quad_box') ):

						while( have_rows('quad_box') ): the_row();

							// vars
							$icon = get_sub_field('diag_icon');
							$text = get_sub_field('diag_text');

						?>
						<div class="col-md-6 mb-5 border-alt">
							<?php if ( get_sub_field( 'diag_icon' ) ): ?>
								<div class="col-6 float-left">

									<img src="<?php echo $icon ?>" alt="<?php echo $text ?>" />

								</div>
							<?php else: endif; ?>
							<?php if ( get_sub_field( 'diag_icon' ) ): ?>
							<div class="col-6 float-right pt-0 pt-md-5">
							<?php else: ?>
							<div class="col-12 float-right pt-0 pt-md-5">
							<?php	endif; ?>
								<?php echo $text ?>
							</div>
						</div>

						<?php endwhile; ?>

					<?php endif; ?>
				</div>
			</div>
		</div>

		<!--- Research CTA --->

		<?php if( have_rows('research_cta') ):

			while( have_rows('research_cta') ): the_row();

				// vars
				$title = get_sub_field('research_cta_title');
				$btnlabel = get_sub_field('research_cta_button_label');
				$link = get_sub_field('research_cta_button_link');
				$color = get_sub_field('research_cta_background_color');

		?>

		<div class="row mt-5 mb-5" style="background-color: <?php echo $color ?>;">
				<div class="col-12 col-md-8 pl-md-5 ml-md-5 text-white cta-title">
						<?php echo $title ?>
				</div>
				<div class="col-12 col-md-3 mb-3 mb-md-0 d-flex align-items-center">
						<a href="<?php echo $link ?>" class="btn btn-secondary btn-lg cta-btn col-sm-12" style="color: <?php echo $color ?>" title="<?php echo $btnlabel ?>">
								<?php echo $btnlabel ?>
						</a>
				</div>
		</div>

		<?php endwhile; ?>

		<?php endif; ?>


<!--- Testimonial Carousel --->
	<div class="container">
		<div class="carousel" data-flickity='{ "wrapAround": true }' style="margin-top: 40px;">
		  <div class="carousel-cell">
				<div class="float-left d-flex carousel-cell-image" style="background-image:url('<?php echo get_site_url(); ?>/wp-content/uploads/2017/09/mike-welsh.jpg')">

					&nbsp;
				</div>
				<div class="float-right carousel-cell-text p-3">
					<strong>Mike Welsh<br />Learning &amp; Development Partner, Facebook</strong>
					<p>"Completing the Leader Network Diagnostic was fascinating. The videos gave me a depth of understanding of the theory. The diagnostic led me down a path of introspection, leading me to insights into why the strengths and weaknesses of my network exist and to clear, actionable insights of how to improve my network."</p>
				</div>
			</div>
            <div class="carousel-cell">
				<div class="float-left d-flex carousel-cell-image" style="background-image:url('<?php echo get_site_url(); ?>/wp-content/uploads/2017/09/esther-banda.jpg')">

					&nbsp;
				</div>
				<div class="float-right carousel-cell-text p-3">
					<strong>Esther Banda<br />Empathy in Enterprise, Harare, Harare, Zimbabwe</strong>
					<p>"I learned that my network is fairly open but I have one person who seems to be in all areas of my life and know almost everyone else. I need to create deeper relationships."</p>
				</div>
			</div>
            <div class="carousel-cell">
				<div class="float-left col-sm-3 carousel-cell-image" style="background-image:url('<?php echo get_site_url(); ?>/wp-content/uploads/2017/09/nick-petrie.jpg')">

					&nbsp;
				</div>
				<div class="float-right col-sm-9 carousel-cell-text p-3">
					<strong>Nick Petrie<br />Learning &amp; Senior Faculty, Center for Creative Leadership</strong>
					<p>"30 minutes using this tool changed the way I think about networks and made me a better leader. This is a tool that every leader should use if they care about improving their effectiveness."</p>
				</div>
			</div>
		</div>
	</div>

		<!--- Video Section --->

		<?php if( have_rows('diag_video') ):

			while( have_rows('diag_video') ): the_row();

							// vars
							$background = get_sub_field('diag_vid_background');
							$title = get_sub_field('diag_vid_title');
							$btn_text = get_sub_field('diag_vid_btn_text');
							$btn_url = get_sub_field('diag_vid_btn_url');

			?>

			<div class="row mt-5 mb-5 pt-5 pb-5 diag-video" style="background-image: url('<?php echo $background ?>');">

					<div class="container">
						<div class="row">
							<div class="col-md-6 mt-5 mb-5 pt-5 pb-5">
								<h2 class="pt-5 pb-5 text-white"><?php echo $title ?></em></h2>
								<a href="<?php echo $btn_url ?>" class="btn-primary btn-lg text-white mt-3 mb-5 p-3 wplightbox"><?php echo $btn_text ?></a>
							</div>
						</div>
					</div>
					<?php endwhile; ?>

				<?php endif; ?>

			</div>

		<!--- Pricing CTA --->

		<?php if( have_rows('pricing_cta') ):

			while( have_rows('pricing_cta') ): the_row();

				// vars
				$title = get_sub_field('pricing_cta_title');
				$btnlabel = get_sub_field('pricing_cta_button_label');
				$link = get_sub_field('pricing_cta_button_link');
				$color = get_sub_field('pricing_cta_background_color');

		?>

		<div class="row mt-5 mb-5" style="background-color: <?php echo $color ?>;">
				<div class="col-12 col-md-8 pl-md-5 ml-md-5 text-white cta-title">
						<?php echo $title ?>
				</div>
				<div class="col-12 col-md-3 mb-3 mb-md-0 d-flex align-items-center">
						<a href="<?php echo $link ?>" class="btn btn-secondary btn-lg cta-btn col-sm-12" style="color: <?php echo $color ?>" title="<?php echo $btnlabel ?>">
								<?php echo $btnlabel ?>
						</a>
				</div>
		</div>

		<?php endwhile; ?>

		<?php endif; ?>




	</div><!-- .entry-content -->

</article><!-- #post-## -->
