<?php
/**
 * Partial template for content in home.php
 *
 * @package understrap
 */

?>
<article <?php post_class(); ?> id="post-
    <?php the_ID(); ?>">

    <!--- Main content area --->
    <div class="container pt-5">
        <div class="row justify-content-center pb-5">
            <?php the_content(); ?>
        </div>
    </div>

    <!--- Service Boxes --->
    <div class="container mt-5 mb-5">
        <div class="row">
            <?php if( have_rows( '3_colored_boxes') ): while( have_rows( '3_colored_boxes') ): the_row();
            // vars
            $title=get_sub_field( '3_box_title');
            $icon=get_sub_field( '3_box_icon');
            $excerpt=get_sub_field( '3_box_excerpt');
            $color=get_sub_field( '3_box_color');
            $hover_color=get_sub_field( '3_box_hover_color');
            $link=get_sub_field( '3_box_link');
            ?>

            <div class="col-md-11 col-lg ml-3 mr-3 p-3 mb-2 icon-mobile" style="background: <?php echo $color ?> url('<?php echo $icon ?>') no-repeat right 0px; background-size: 35%;">
                <a href="<?php echo $link ?>" title="<?php echo $title ?>" style="text-decoration: none">
                    <h2 class="mb-3" style="color: #fff; text-transform: uppercase; font-weight: bold;" ><?php echo $title ?></h2>
                    <div class="colored-boxes">
                        <?php echo $excerpt ?>
                    </div>
                </a>
            </div>

            <?php endwhile; ?>

            <?php endif; ?>
        </div>
    </div>

    <!--- Network Patterns 4 box grid --->
    <div class="container">
        <div class="row justify-content-center pt-5">

            <?php the_field( '4_box_header_text'); ?>
            <div class="row justify-content-center mt-5">
                <?php if( have_rows( '4_network_patterns') ): while( have_rows( '4_network_patterns') ): the_row();

                // vars
                    $icon=get_sub_field( '4_box_icon');
                    $title=get_sub_field( '4_box_title');
                ?>
                <div class="col-6 col-sm-3 col-md-4 col-lg-2 ml-4 mr-5 mb-4">
                    <img src="<?php echo $icon ?>" alt="<?php echo $title ?>" class="mb-4" /><h5 class="text-center" style="font-weight: normal"><?php echo $title ?></h5>
                </div>

                <?php endwhile; ?>

                <?php endif; ?>
            </div>
        </div>
    </div>

    <!--- Data Driven Grid --->
    <div class="container">
        <div class="row mt-5 pt-5 mb-5">

            <?php if( have_rows( '3rd_section') ): while( have_rows( '3rd_section') ): the_row();

            // vars
                $title=get_sub_field( '3rd_title');
                $content=get_sub_field( '3rd_content_right');

            ?>
            <div class="col-md-4 bottom-border">
                <h1 class="p-4 text-center data-title"><?php echo $title ?></h1>
            </div>
            <div class="col-md-8 pl-5-md mobile-bullets">
                <span style="font-size: 20px;"><?php echo $content ?></span>
            </div>
            <?php endwhile; ?>

            <?php endif; ?>

        </div>
    </div>

    <!--- Map Section (No Container DIV for Edge-to-Edge in BS4alpha) --->

    <div class="row justify-content-center pt-5 pb-5" style="min-height: 600px">

        <?php if( have_rows( 'map_section') ): while( have_rows( 'map_section') ): the_row(); // vars
            $title=get_sub_field( 'map_title');
            $map=get_sub_field( 'map_image');
        ?>
        <div class="col map-background" style="background: #EAEAE9 url('<?php echo $map ?>') no-repeat center center; background-size: cover">
            <?php echo $title ?>
        </div>
        <?php endwhile; ?>

        <?php endif; ?>

    </div>

    <!--- Video Section --->

    <?php if( have_rows( 'video_section') ): while( have_rows( 'video_section') ): the_row(); // vars
        $background=get_sub_field( 'background');
        $testimonial=get_sub_field( 'testimonial');
        $name=get_sub_field( 'name');
        $title=get_sub_field( 'testimonial_title');
        $btn_text=get_sub_field( 'btn_text');
        $video=get_sub_field( 'video');
    ?>

    <div class="row mt-5 mb-5 pt-5 pb-5 testimonial" style="background-image: url('<?php echo $background ?>');">

        <div class="container">
            <div class="row">
                <div class="col-md-7 pt-5 pb-5">
                    <h1 style="font-weight: normal" class="testimonial-white">"<?php echo $testimonial ?>"</h1>
                    <h2 class="pt-2"><em>- <?php echo $name ?></em></h2>
                    <h3><?php echo $title ?></h3>
                    <br />
                    <a href="<?php echo $video ?>" class="btn-primary btn-lg text-white wplightbox">
                        <?php echo $btn_text ?>
                    </a>
                </div>
                <div class="col-md-3 pt-5 pb-5">
                  <a href="<?php echo $video ?>" class="wplightbox">
                    <!--- Play Icon Here Maybe --->
                  </a>
                </div>
            </div>
        </div>
        <?php endwhile; ?>

        <?php endif; ?>

    </div>

</article>
<!-- #post-## -->
