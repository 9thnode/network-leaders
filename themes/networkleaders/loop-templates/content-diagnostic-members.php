<?php
/**
 * Partial template for content in diagnostic.php
 *
 * @package understrap
 */
$upload_dir = wp_upload_dir();
?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<div class="entry-content">
		<?php if( have_rows('trainer_3rd_section') ):

			while( have_rows('trainer_3rd_section') ): the_row();

					// vars
					$title = get_sub_field('trainer_3rd_title');
					$icon = get_sub_field('trainer_3rd_icon');
					$copy = get_sub_field('trainer_3rd_text');

				?>
        <!--- Data Driven Grid --->
        <div class="container">
            <div class="row mt-5 pt-5 mb-5 d-flex text-center justify-content-around">

                <div class="col-md-6 bottom-border text-center pt-5" style="max-width: 400px; line-height: 4em;">
                    <?php echo $title ?>
                </div>
                <div class="col-md-6 pl-5-md mobile-bullets">

                        <img src="<?php echo $icon ?>"  style="width:200px;" />

                </div>

            </div>
        </div>

			<?php endwhile; ?>

		<?php endif; ?>
			<div class="container">
				<div class="row justify-content-center pb-5">
					<?php echo $copy ?>
				</div>
			</div>


			<!--- Let's Get Started --->
			<div class="row justify-content-center pt-5 ">
					<div class="col-10 text-center">
							<h3 style="font-weight: bold;"><?php the_field( 'section_title'); ?></h3>
					</div>
			</div>
			<div class="container text-center" >
					<?php the_field( 'section_copy'); ?>
			</div>
			<?php if( have_rows('download_buttons') ):

				while( have_rows('download_buttons') ): the_row();

						// vars
						$diagnostic_btn = get_sub_field('download_diagnostic_btn');
						$interpretation_guide_btn = get_sub_field('download_interpretation_guide_btn');

					?>


	        <div class="container mt-5 mb-5">
	            <div class="row d-flex text-center justify-content-around">
								<div class="col-12 col-md-6 mb-4 mb-md-0"><a class="btn-blue text-white btn-block" href="<?php echo $diagnostic_btn ?>" target="_blank">Download the Diagnostic</a></div>
								<div class="col-12 col-md-6"><a class="btn-green text-white btn-block" href="<?php echo $interpretation_guide_btn ?>" target="_blank">Download the Interpretation Guide</a></div>
	            </div>
	        </div>

				<?php endwhile; ?>

			<?php endif; ?>


        <!--- What You'll Need --->
        <div class="<?php echo esc_attr( $container ); ?>">
            <div class="row justify-content-center pt-5 pb-5">
                  <h3 style="font-weight: bold;"><?php the_field( 'section_title_2'); ?></h3>
						</div>
                <div class="row justify-content-center pb-5">
                    <?php if( have_rows( '3_col_icons') ): while( have_rows( '3_col_icons') ): the_row();
                    // vars

                        $icon=get_sub_field( 'col_icon');
                        $subtitle=get_sub_field( 'col_subtitle');
                    ?>
                    <div class="col-10 col-md-4 col-lg-auto mb-4 pl-5 pr-5 text-center">

                        <img src="<?php echo $icon ?>" alt="<?php echo $title ?>" class="mb-4 icon" />
                        <h5 style="font-weight: normal"><?php echo $subtitle ?></h5>
                    </div>

                    <?php endwhile; ?>

                    <?php endif; ?>
                </div>
        </div>
        <div class="container text-center" >
            <?php the_field( 'section_copy_2'); ?>
        </div>

				<!--- Video Rows --->
							<?php if( have_rows( 'step_video_section') ): while( have_rows( 'step_video_section') ): the_row();

							// vars
									$steptitle=get_sub_field( 'step_section_title');
									$videodetails=get_sub_field( 'video_details');
									$stepicon=get_sub_field( 'step_icon');
									$stepdirections=get_sub_field( 'step_directions');

									?>
                <!--- Video Steps--->
                <div class="row video-step mt-5 mb-5">
									<!--- Video Row--->
                    <div class="container justify-content-around mt-2 pt-2 pb-3 mb-3">

											<div class="row justify-content-center">

                        <div class="col-md-12 p-2">
                        		<center><?php echo $steptitle ?></center>
                        </div>
													<!--- Individual Video --->
													<?php if( have_rows( 'step_video_details') ): while( have_rows( 'step_video_details') ): the_row();

													// vars

															$videotitle=get_sub_field( 'video_title');
															$videothumb=get_sub_field( 'video_thumbnail');
															$video=get_sub_field( 'video_file');
															$videometa=get_sub_field( 'video_meta');
															$videotime=get_sub_field( 'video_time');
															?>

															<div class="d-flex p-4">
			                            <div class="video">
			                                <a href="<?php echo $video ?>" class="wplightbox"><img src="<?php echo $videothumb ?>" style="max-width: 300px;" /></a>
			                                <div class="diag-caption clearfix pt-2 pl-3 pr-3 pb-3">
			                                    <div class="float-left"><strong><?php echo $videotitle ?></strong></div><br />
																					<div><p class="float-left mt-2" style="font-size: .8rem;"><?php echo $videometa ?> - (<?php echo $videotime ?>)</p></div>
			                                </div>
			                            </div>
			                        </div>

													<?php endwhile; ?>

				                  <?php endif; ?>

													<!--- Individual Video --->
														<span class="divider"></span>

                        <div class="d-flex flex-row justify-content-end pt-2 pl-3 ml-2 pl-5-md status-box">
                            <div class="border m-3 p-3 text-center">
                                <img src="<?php echo $stepicon ?>" />
                                <div class="p-3 text-center data-title stepdirections"><?php echo $stepdirections ?></h2>
                            		</div>
                        		</div>
                    		</div>
											</div>
                </div>
                 <!--- Video Steps--->
						 		</div>
							 <?php endwhile; ?>

							 <?php endif; ?>


                <!--- Downloads --->
                <div class="container pb-5 mt-5">

                        <?php if( have_rows( 'download_section') ): while( have_rows( 'download_section') ): the_row();
                        // vars
                            $icon=get_sub_field( 'download_icon');
                            $title=get_sub_field( 'download_title');
                            $content=get_sub_field( 'download_description');
														$btn_text=get_sub_field('download_button_text');
                            $link=get_sub_field( 'download_button_link');
                            $color=get_sub_field( 'color');
                            $hover_color=get_sub_field( 'hover_color');
                        ?>
                        <div class="row mt-2 pt-2 mb-2 d-flex">
                            <div class="col-md-4 bottom-border text-center">
                                <img class="m-5" src="<?php echo $icon ?>" style="width: 150px; margin: 0 auto;" />
                            </div>
                            <div class="col-md-8 pl-5-md mobile-bullets">
                                <h2 class="p-4 text-center data-title"><?php echo $title ?></h2>
                                <span style="font-size: 20px;"><?php echo $content ?></span>
																<div class="col mb-4 mb-md-0 mt-3"><a href="<?php echo $link ?>" class="btn-block p-3" style="background: <?php echo $color ?>; border-color: transparent; color:#fff;" ><center><?php echo $btn_text ?></center></a></div>
                            </div>
                        </div>

                        <?php endwhile; ?>

                        <?php endif; ?>


                </div>
                <!--- Downloads --->

            </div>

					<div class="row mt-5 justify-content-center">
						<div class="d-flex p-4">
								<div class="video">
										<a href="<?php echo site_url(); ?>/wp-content/uploads/2017/11/Resource-Traps.m4v" class="wplightbox"><img src="<?php echo site_url(); ?>/wp-content/uploads/2017/12/LND-Traps-01-Resource.jpg" style="max-width: 300px;" /></a>
										<div class="diag-caption clearfix pt-2 pl-3 pr-3 pb-3">
												<div class="float-left"><strong>Part 1 - Resource Traps</strong></div><br />
												<div><p class="float-left mt-2" style="font-size: .8rem;">Video 1 of 3 - (6:10)</p></div>
										</div>
								</div>
						</div>

						<div class="d-flex p-4">
								<div class="video">
										<a href="<?php echo site_url(); ?>/wp-content/uploads/2017/11/Structural-Traps.m4v" class="wplightbox"><img src="<?php echo site_url(); ?>/wp-content/uploads/2017/12/LND-Traps-02-Structural.jpg" style="max-width: 300px;" /></a>
										<div class="diag-caption clearfix pt-2 pl-3 pr-3 pb-3">
												<div class="float-left"><strong>Part 2 - Structural Traps</strong></div><br />
												<div><p class="float-left mt-2" style="font-size: .8rem;">Video 2 of 3 - (4:40)</p></div>
										</div>
								</div>
						</div>

						<div class="d-flex p-4">
								<div class="video">
										<a href="<?php echo site_url(); ?>/wp-content/uploads/2017/11/Developmental-Traps.m4v" class="wplightbox"><img src="<?php echo site_url(); ?>/wp-content/uploads/2017/12/LND-Traps-03-Developmental.jpg" style="max-width: 300px;" /></a>
										<div class="diag-caption clearfix pt-2 pl-3 pr-3 pb-3">
												<div class="float-left"><strong>Part 3 - Development Traps</strong></div><br />
												<div><p class="float-left mt-2" style="font-size: .8rem;">Video 3 of 3 - (3:33)</p></div>
										</div>
								</div>
						</div>
					</div>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
