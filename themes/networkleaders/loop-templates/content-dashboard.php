<?php
/**
* Partial template for content in certified-trainer.php *
* @package understrap */
$container = get_theme_mod( 'understrap_container_type' );
?>
<article <?php post_class(); ?> id="post-
    <?php the_ID(); ?>">

    <div class="entry-content">

      <div class="container">
          <div class="row justify-content-center pb-2">

          <?php if(current_user_can('mepr-active','membership:308')): ?>
            <div class="alert alert-info" role="alert">
              <h4 class="alert-heading"><strong>Welcome!</strong></h4>
              <p>Now that you’re a Certified Trainer you have access to <strong><em><u>discount licensing of individual diagnostics</u></em></strong>. These are available in bullk and discounts increase for larger purchases.</p>
              <a class="btn btn-primary ml-2" href="<?php echo site_url(); ?>/plans/license-bundles/" role="button" style="padding: 10px 50px;">Licensing</a>
            </div>
          <?php endif; ?>
          </div>

      </div>

      <div class="container">
          <div class="row justify-content-center pb-5">
                <?php the_content(); ?>
          </div>
      </div>

      <!--- Contact Form Popup --->
      <div class="modal fade" id="contactus" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="contactLabel">Contact Us!</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col"><?php echo do_shortcode('[ninja_form id=4]'); ?></div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>


    </div>
    <!-- .entry-content -->

</article>
<!-- #post-## -->
