<?php
/**
 * Partial template for content in research.php
 *
 * @package understrap
 */
$container = get_theme_mod( 'understrap_container_type' );
?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<div class="entry-content">

		<!--- Top Image Left --->
		<div class="<?php echo esc_attr( $container ); ?>">
			<div class="row">
				<div class="col-12 col-md-6 col-lg-4">
					<img src="<?php echo get_field('left_image') ?>" title="The Research" alt="The Research - Left" />
				</div>
				<div class="col-12 col-md-6 col-lg-8 mt-3">
					<?php echo get_field('left_image_copy')?>
				</div>
			</div>
		</div>

		<!--- Middle Tagline --->
		<div class="<?php echo esc_attr( $container ); ?>">
			<div class="row mt-5 mb-5 pt-5 pb-5 text-center">
				<?php echo get_field('middle_section_tagline') ?>
			</div>
		</div>

		<!--- 2 box grid --->
		<div class="<?php echo esc_attr( $container ); ?>">
			<div class="row mb-5">

				<?php if( have_rows('2_box_group') ):

					while( have_rows('2_box_group') ): the_row();

						// vars
						$icon = get_sub_field('research_2_box_icon');
						$color = get_sub_field('research_sidebar_color');
						$copy = get_sub_field('research_2_box_copy');

					?>

						<div class="col-12 col-md-1 color-sidebar" style="background-color: <?php echo $color ?>">
							<img src="<?php echo $icon ?>" class="p-1 mt-2" />
						</div>
						<div class="col-12 col-md-5" style="margin-bottom: 20px;">
							<?php echo $copy ?>
						</div>

					<?php endwhile; ?>

				<?php endif; ?>

			</div>
		</div>

		<!--- Middle Image Right --->
		<div class="<?php echo esc_attr( $container ); ?>">
			<div class="row mt-5 pt-5">
				<div class="col-12 col-md-6 col-lg-8 order-2 order-md-1">
					<?php echo get_field('right_image_copy')?>
				</div>
				<div class="col-12 col-md-6 col-lg-4 order-1 order-md-2 mb-3 mb-md-0">
					<img src="<?php echo get_field('right_image') ?>" title="The Research" alt="The Research - Right" />
				</div>
			</div>
		</div>

		<!--- Pie Chart Section --->
		<div class="<?php echo esc_attr( $container ); ?>">
			<div class="row mt-5 pt-5">

				<?php if( have_rows('pie_chart') ):

					while( have_rows('pie_chart') ): the_row();

						// vars
						$icon = get_sub_field('pie_chart_icon');
						$title = get_sub_field('pie_chart_title');

					?>
						<div class="col-12 col-md-2 pl-md-5">
							<img src="<?php echo $icon ?>" alt="<?php echo $title ?>" class="mb-4"/></h2>
						</div>
						<div class="col-12 col-md-10 pl-md-5 mt-3">
							<?php echo $title ?>
						</div>

					<?php endwhile; ?>

				<?php endif; ?>

			</div>
		</div>

		<!--- 3rd Party Link Boxes --->
		<div class="container">
			<div class="row pt-5 pb-5 mt-5 mb-5">

				<?php if( have_rows('3rd_party_links') ):

					while( have_rows('3rd_party_links') ): the_row(); ?>

						<div class="text-center pb-5"><?php echo get_sub_field('3rd_party_title') ?></div>

						<?php if( have_rows('3rd_party_button_links') ):

							while( have_rows('3rd_party_button_links') ): the_row();

							// vars
							$title = get_sub_field('3rd_party_link_titles');
							$link = get_sub_field('3rd_party_link');

						?>
						<div class="col-12 col-md-4 p-1">
							<a href="<?php echo $link ?>" title="<?php echo $title ?>" target="_blank"><button class="button-links"><?php echo $title ?></button></a>
						</div>

							<?php endwhile; ?>

						<?php endif; ?>

					<?php endwhile; ?>

				<?php endif; ?>

				<div class="col-12 text-center pt-5"><p><strong>Click any of the boxes above to learn more</strong></p></div>

			</div>
		</div>



		<!--- Pricing CTA --->

    <?php if( have_rows('pricing_cta') ):

			while( have_rows('pricing_cta') ): the_row();

				// vars
				$title = get_sub_field('pricing_cta_title');
				$btnlabel = get_sub_field('pricing_cta_button_label');
				$link = get_sub_field('pricing_cta_button_link');
				$color = get_sub_field('pricing_cta_background_color');

		?>

			<div class="row" style="background-color: <?php echo $color ?>;">
			  <div class="col-12 col-md-8 pl-md-5 ml-md-5 text-white cta-title"><?php echo $title ?></div>
			  <div class="col-12 col-md-3 d-flex align-items-center pb-3"><a href="<?php echo $link ?>" class="btn btn-secondary btn-lg cta-btn col-sm-12" style="color: <?php echo $color ?>" title="<?php echo $btnlabel ?>"><?php echo $btnlabel ?></a></div>
			</div>

		<?php endwhile; ?>

		<?php endif; ?>

		<!--- Video Section --->

		<?php if( have_rows('research_video') ):

			while( have_rows('research_video') ): the_row();

							// vars
							$background = get_sub_field('research_vid_background');
							$title = get_sub_field('research_vid_title');
							$btn_text = get_sub_field('research_vid_btn_text');
							$btn_url = get_sub_field('research_vid_btn_url');

			?>

			<div class="row mt-5 mb-5 pt-5 pb-5 research-video" style="background-image: url('<?php echo $background ?>');">

					<div class="container">
						<div class="row">
							<div class="col-md-6 mt-5 mb-5 pt-5 pb-5">
								<h2 class="pt-5 pb-5 text-white" style="text-shadow:2px 2px 8px rgba(100, 100, 100, 1);"><?php echo $title ?></em></h2>
								<a href="<?php echo $btn_url ?>" class="btn-primary btn-lg text-white mt-3 mb-5 p-3 wplightbox"><?php echo $btn_text ?></a>
							</div>
						</div>
					</div>
					<?php endwhile; ?>

				<?php endif; ?>

			</div>

		<!--- Philip Willburn --->
		<div class="<?php echo esc_attr( $container ); ?>">
			<div class="row" id="philip">

				<div class="container">
					<div class="row pt-5 pb-5 mt-5 mb-5">
						<div class="col-12 col-md-5 col-lg-4 pr-5">
							<img src="<?php echo get_field('philip_image') ?>" title="Philip Willburn" alt="Philip Willburn" />
						</div>
						<div class="col-12 col-md-7 col-lg-8 mt-4 mt-md-0">
							<?php echo get_field('philip_bio_copy')?>
						</div>
					</div>
				</div>

			</div>
		</div>

		<!--- Citations --->
		<div class="citations row">
			<div class="pt-5 pb-5 mt-5 mb-5 container">

				<?php if( have_rows('research_sources') ):

					while( have_rows('research_sources') ): the_row();

							// vars
							$title = get_sub_field('research_sources_title');
							$link = get_sub_field('research_sources_body');

						?>
						<div class="pb-2"><?php echo get_sub_field('research_sources_title') ?></div>
						<div class="sources"><?php echo get_sub_field('research_sources_body') ?></div>

					<?php endwhile; ?>

				<?php endif; ?>

			</div>
		</div>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
