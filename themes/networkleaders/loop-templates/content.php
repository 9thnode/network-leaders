<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header container-fluid m-0 pt-3 pb-3 pl-0 pr-0">

		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
		'</a></h2>' ); ?>

		<?php if ( 'post' == get_post_type() ) : ?>

			<div class="entry-meta m-0 p-3">
				<?php understrap_posted_on(); ?>
			</div><!-- .entry-meta -->

		<?php endif; ?>

	</header><!-- .entry-header -->
    <div class="row">
        <div class="col-sm-12 mb-3">
           <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
        </div>
        <div class="col-sm-12">
            <div class="entry-content">

                <?php
                the_excerpt();
                ?>

                <?php
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
                    'after'  => '</div>',
                ) );
                ?>

            </div><!-- .entry-content -->
        </div>
    </div>
	<footer class="entry-footer container-fluid m-0 pt-3 pb-3">
        <div>
		  <?php understrap_entry_footer(); ?>
        </div>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
