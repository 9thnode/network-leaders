<?php /**

* The header for our theme. *
* Displays all of the <head> section and everything up till <div id="content">
    * *   @package understrap
    */
$container = get_theme_mod( 'understrap_container_type' );
$upload_dir = wp_upload_dir();

if ( get_field( 'hero_image' ) ):
  $hero_image = get_field('hero_image') ;
else:
  $hero_image = site_url("/wp-content/uploads/2017/11/hero-home.png") ;
endif;

?>
    <!DOCTYPE html>
    <html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <link rel="stylesheet" href="https://unpkg.com/flickity@2.0/dist/flickity.min.css">
        <script src="https://unpkg.com/flickity@2.0/dist/flickity.pkgd.min.js"></script>
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>

        <div class="hfeed site" id="page">

            <!-- ******************* The Navbar Area ******************* -->

            <div class="wrapper-fluid wrapper-navbar sticky-top" id="wrapper-navbar">

                <a class="skip-link screen-reader-text sr-only" href="#content">
                    <?php esc_html_e( 'Skip to content', 'understrap' ); ?>
                </a>

                <nav class="navbar navbar-expand-lg sticky-top">
                    <div class="<?php echo esc_attr( $container ); ?>">
                        <div class="d-inline-flex justify-content-between col-lg-3">
                            <!-- Your site title as branding in the menu -->
                            <?php if ( ! has_custom_logo() ) { ?>

                            <?php if ( is_front_page() && is_home() ) : ?>
                            <h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
                            <?php else : ?>

                            <a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                                  <?php bloginfo( 'name' ); ?>
                            </a>

                            <?php endif; ?>
                            <?php } else { the_custom_logo(); } ?>
                            <!-- end custom logo -->

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </div>
                        <div class="d-inline-flex justify-content-between col-lg-9">
                            <!-- The WordPress Menu goes here -->
                            <?php wp_nav_menu( array( 'theme_location'=> 'primary', 'container_class' => 'collapse navbar-collapse justify-content-end', 'container_id' => 'navbarNavDropdown', 'menu_class' => 'navbar-nav', 'fallback_cb' => '', 'menu_id' => 'main-menu', 'walker' => new WP_Bootstrap_Navwalker(), ) ); ?>
                        </div>
                    </div>
                    <!-- .container -->
                </nav>
                <!-- .site-navigation -->

            </div>
            <!-- .wrapper-navbar end -->

            <!-- Custom Header Backgrounds - Titles - Pricing - Social Media -->
              <div class="hero justify-content-center" style="background-image: url('<?php echo $hero_image ?>')">

                <?php if ( is_page( 'the-diagnostic-content')) {
                  echo '<div class="hero-items-diag container">';
                    } else {
                  echo '<div class="hero-items container">'; } ?>

                    <div class="row">

                      <?php if( get_field( 'hero_text') ) { ?>
                        <div class="col col-md-10 col-xl-6 text-white hero-text">
                          <?php the_field( 'hero_text'); ?>
                        </div>

                      <?php } elseif (is_singular('memberpressproduct')) { ?>

                      <?php } elseif (is_singular('memberpressgroup')) { ?>

                      <?php } elseif (is_author('admin')) { ?>

                      <?php } elseif (is_page ('dashboard')) { ?>
                        <header class="col entry-header ml-md-4">
                          <h1 class="entry-title"> Getting <strong>Started</strong></h1>
                        </header>
                      <?php } else { ?>

                      <?php if ( is_page( 'certified-trainer')) {
                        echo '<div class="col">';
                          } else {
                        echo '<div class="col-md-2">'; } ?>

                      <header class="entry-header ml-md-4">
                        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                      </header>

                      </div>

                      <?php } ?>

                  <div class="col d-inline align-self-end">
                    <?php if ( get_field( 'pricing_button' ) ): ?>
                      <!-- Toggle Pricing Button -->
                        <div class="col-sm-12 p-2 d-sm-inline-flex justify-content-end">
                          <center>
                            <a href="<?php echo site_url(); ?>/plans/select/" class="btn pricing mb-0">PRICING</a>
                          </center>
                        </div>
                    <?php else: endif; ?>


                    <?php if( get_field( 'enable_social_icons') ) { ?>
                      <!-- Toggle Social Media Icons -->
                        <div class="col social p-2 d-sm-inline-flex justify-content-end">
                          <center>
                            <a href="https://twitter.com/NetworkLeader1" target="_blank">
                              <span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x">
                                  <i class="fa fa-twitter fa-stack-1x"></i>
                                </i>
                              </span>
                            </a>
                            <a href="https://www.facebook.com/Network-Leader-496503940709556/" target="_blank">
                              <span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x">
                                  <i class="fa fa-facebook fa-stack-1x"></i>
                                </i>
                              </span>
                            </a>
                            <a href="https://www.linkedin.com/in/philwillburn/" target="_blank">
                              <span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x">
                                  <i class="fa fa-linkedin fa-stack-1x"></i>
                                </i>
                              </span>
                            </a>
                          </center>
                        </div>
                    <?php } else { echo ""; } ?>
                  </div>
                </div>
            </div>
        </div>
