<?php if(!defined('ABSPATH')) {die('You are not allowed to call this page directly.');}

class MPCA_Subscription_Controller {
  public function __construct() {
    add_action('mepr-event-subscription-changed', array($this,'subscription_changed'));
  }

  public function subscription_changed($e) {
    $old_txn = $e->get_data();
    $new_txn_id = $e->get_args();

    if(empty($new_txn_id)) { return; }

    $new_txn = new MeprTransaction($new_txn_id);

    if(!($old_sub = $old_txn->subscription())) {
      $old_sub = $old_txn;
    }

    if(!($new_sub = $new_txn->subscription())) {
      $new_sub = $new_txn;
    }

    if(($old_ca = MPCA_Corporate_Account::find_corporate_account_by_obj($old_sub)) &&
       ($new_ca = MPCA_Corporate_Account::find_corporate_account_by_obj($new_sub)) &&
       $old_ca->is_enabled() && $new_ca->is_enabled()) {
      $new_ca->copy_sub_accounts_from($old_ca);
    }
  }
}
